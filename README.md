# react-shop

Костенко Ксения

[Задание](ТЗ.ReactJS.pdf)

## Описание проекта

* SPA-приложение на ReactJS + React Hooks
* Redux ([redux-toolkit](https://redux-toolkit.js.org/))
* Стилизация - препроцессор [Scss](https://sass-lang.com/)
* Именование классов по [БЭМ](https://ru.bem.info/)
* Используется [Webpack](https://webpack.js.org/) для разворачивания проекта и сборки JavaScript и CSS

## Установки

* Установите [NodeJS](https://nodejs.org/en/) ниже версии 16.x и [npm](https://www.npmjs.com/) ниже версии 7.x


## Запуск проекта

```bash
npm i
npm run start
```

## Команды

* `npm run start` - запуск сервера для разработки
* `npm run build` - собрать проект без запуска сервера
