var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const miniCss = require('mini-css-extract-plugin');
const CleanTerminalPlugin = require('clean-terminal-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                          name: '[path][name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.scss$/i,
                use: [
                    miniCss.loader,
                    // "style-loader",
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                outputStyle: "compressed",
                            },
                        },
                    },
                ],
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                use: [
                  {
                    loader: "file-loader",
                    options: {
                      name: "[path][name].[ext]",
                    },
                  }
                ],
            },
        ]
    },
    mode: 'development',
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new miniCss({
            filename: '[name].css',
         }),
         new CleanTerminalPlugin(),
    ],
    resolve: {
        extensions: [".js"],
        alias: {
          "@": path.resolve(__dirname,'./'),
          "@assets": path.resolve(__dirname,'./src/assets'),
        },
      },
}