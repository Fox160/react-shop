const reducers = {
  addToCart: (state, { payload: id }) => {
    state[id] = state[id] ? state[id] + 1 : 1;

    return state;
  },
  removeFromCart: (state, { payload: id }) => {
    delete state[id];

    return state;
  },
  clearCart: () => {},
};

export default reducers;
