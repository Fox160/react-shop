import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

const { actions, reducer } = createSlice({
  name: 'cart',
  initialState: {},
  reducers,
});

export const { addToCart, removeFromCart, clearCart } = actions;

export default reducer;
