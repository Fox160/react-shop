import { createSlice } from "@reduxjs/toolkit";
import reducers from "./reducers";

export const {actions, reducer} = createSlice(
  {
    name: 'compare',
    initialState: [],
    reducers
  }
)

export const { addToCompare, removeFromCompare } = actions;

export default reducer;
