import { createSelector } from 'reselect';

const selectCompare = (state) => state.compare;

export const selectCompareItem = createSelector(
  (state) => state.compare,
  (_, id) => id,
  (compare, id) => compare.filter((compareItem) => compareItem === id)[0]
);

export default selectCompare;
