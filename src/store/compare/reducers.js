const reducers = {
  addToCompare: (state, { payload: itemId }) => {
    const index = state.findIndex((e) => e === itemId);

    if (index === -1) {
      state.push(itemId);
    } else {
      state[index] = itemId;
    }

    return state;
  },
  removeFromCompare: (state, { payload: itemId }) => {
    state = state.filter((cartItem) => cartItem !== itemId);

    return state;
  },
};

export default reducers;
