import { current } from 'immer';
import { initialState } from './slice';

const reducers = {
  sortBy: (state, { payload }) => {
    const { type, direction } = payload;

    if (type === '') {
      state = initialState;
    } else {
      let sortedArr = Object.entries(current(state)).sort((a, b) => {
        if (a[1][type] === b[1][type]) {
          return 0;
        }
        return a[1][type] > b[1][type] ? direction : direction * -1;
      });

      state = sortedArr.reduce((acc, [id, value]) => {
        // const [id, value] = el;
        return { ...acc, [id]: { ...value } };
      }, {});
    }

    return state;
  },
  filterAvailable: (state, { payload: value }) => {
    if (!value) {
      state = initialState;
    } else {
      let sortedArr = Object.entries(current(state)).filter(([key, value]) => {
        if (value.available) {
          return { [key]: value };
        }
        return null;
      });

      state = sortedArr.reduce((acc, [id, value]) => {
        return { ...acc, [id]: { ...value } };
      }, {});
    }

    return state;
  },
};

export default reducers;
