import { createSelector } from "reselect";

const selectGoods = (state) => state.goods;

export const selectGoodsItem = createSelector(
  (state) => state.goods,
  (_, id) => id,
  (goods, id) => goods[id]
);

export default selectGoods;
