import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

export const initialState = {
  'd9b4456c-46b2-4df8-8464-0aa59427b2f7': {
    img: 'valmona',
    name: 'VALMONA SUGAR VELVET MILK SHAMPOO',
    price: 300,
    description: 'Шампунь против перхоти',
    available: true,
    options: [
      {
        title: 'Бренд',
        text: 'Valmona',
      },
      {
        title: 'Объем',
        text: '480 мл',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Шампунь с ягодами и молоком от корейского бренда VALMONA для качественного очищения кожи головы.</p><p>Шампунь против перхоти с ягодами и молоком отлично справляется с очищением кожи головы, удалением отмерших клеток эпидермиса и перхоти. Увлажняет кожу, питает корни и при этом не сушит сами волосы.</p><p>Сочетание ягод и молока создает невероятный аромат. Клубника, черника и малина в составе насыщают волосы витаминами. За счет кислот в ягодах средство эффективно обновляет верхний сой эпидермиса, способствуя оздоровлению кожи и росту новых волос. Молочные протеины питают волосяные луковицы, укрепляя волосы до самых кончиков.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'Water, Ammonium Laureth Sulfate, Ammonium Lauryl Sulfate, Cocamidopropyl Betaine, Dimethicone, Laureth-23, Laureth-3, Cocamide MEA, Propylene Glycol, Fragrance, Sodium Benzoate, Guar Hydroxypropyltrimonium Chloride, Glycol Distearate, Disodium EDTA, Sodium Chloride, Polyquaternium-10, Rubus Fruticosus (Blackberry) Fruit Extract, Vaccinium Angustifolium (Blueberry) Fruit Extract, Rubus Idaeus (Raspberry) Fruit Extract, Euterpe Oleracea Fruit Extract, Prunus Avium (Sweet Cherry) Fruit Extract, Citric Acid, Ethylhexylglycerin, Hydrolyzed Corn Protein, Hydrolyzed Wheat Protein, Hydrolyzed Soy Protein, Allantoin, Nonfat Dry Milk, Sucrose, Cocos Nucifera (Coconut) Oil, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Simmondsia Chinensis (Jojoba) Seed Oil, Macadamia Ternifolia Seed Oil, Glycyrrhiza Uralensis (Licorice) Root Extract, Diospyros Kaki Leaf Extract, Morus Alba Bark Extract, Opuntia Coccinellifera Fruit Extract, Rosa Centifolia Flower Water, Sodium PCA, Betaine, Sorbitol, Glycine, Alanine, Proline, Serine, Threonine, Arginine, Lysine, Glutamic Acid, Punica Granatum Fruit Extract, Hydrolyzed Silk, CI 17200, CI 15985, Ceramide NP.',
      },
    ],
  },
  'fc0c7c73-397a-4cfa-bf92-933e979faa00': {
    img: 'holika',
    name: 'HOLIKA HOLIKA ALOE 99% SOOTHING GEL 55ML',
    price: 295,
    description: 'Увлажняющий универсальный гель для лица и тела',
    available: false,
    options: [
      {
        title: 'Бренд',
        text: 'Holika Holika',
      },
      {
        title: 'Объем',
        text: '55 мл',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Восстанавливает, увлажняет, успокаивает и снимает покраснения! Подходит для лица, тела и даже волос. Он всегда придет на помощь, когда кожа нуждается в экспресс увлажнении. И реанимирует, даже если кожа обгорела.</p><p>На очищенную кожу нанесите необходимое количество геля. При необходимости повторите.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'Сок листьев алоэ, экстракт лотоса орехоносного, экстракт центеллы азиатской, экстракт бамбука дикого, экстракт плодов огурца, экстракт листьев кукурузы, экстракт листьев капусты огородной, экстракт плодов арбуза, ПЭГ-60 гидрогенизированное касторовое масло, натрий полиакрилат, карбомер, триэтаноламин, ароматизатор, феноксиэтанол.',
      },
    ],
  },
  'ee7b1d3c-df9c-4114-8f81-f65c6f6e76ad': {
    img: 'ciracle',
    name: 'CIRACLE PORE CONTROL BLACKHEAD OFF SHEET',
    price: 1150,
    description: 'Салфетки для удаления чёрных точек',
    available: true,
    options: [
      {
        title: 'Бренд',
        text: 'Ciracle',
      },
      {
        title: 'Объем',
        text: '35 штук',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Салфетки пропитаны особой эссенцией, которая размягчает чёрные точки и камедоны, а после выводит все загрязнения из кожи. Процедура займёт не больше 30 минут, а в результате кожа будет чистой и гладкой.</p><p>Как это работает? Оставляете салфетку на 10-15 минут на проблемной зоне. Берёте ложечку уно или ватную палочку и убираете все-все чёрные точки, загрязнения и прыщики. Такой способ очищения не вызывает раздражения кожи и не повреждает эпидермис.</p><p>В упаковке 35 салфеток и специальный пинцет. Им легко взять салфетку из баночки, чтобы не заносить ненужных бактерий в эссенцию.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'Очищенная вода, вода (гидролат) гамамелиса виргинского, экстракт омелы, экстракт шалфея лекарственного, экстракт листьев мяты, экстракт мелиссы, экстракт ягод можжевельника, бутиленгликоль, Лопух обыкновенный, феноксиэтанол, экстракт листьев/корней мыльнянки лекарственной, Метилпарабен, экстракт зверобоя продырявленного, PPG-1-PEG-9, Эфир лаурил гликоль, кокет-7, альфа-бисаболол, аллантоин, ПЭГ-60 гидрогенизированное касторовое масло, глицирризат калия, масло кожуры апельсина, аскорбиновая кислота.',
      },
    ],
  },
  'f7c2db2d-f7e1-47f7-9efa-073ff0501c60': {
    img: 'haruharu',
    name: 'HARUHARU WONDER BLACK RICE HYALURONIC ANTI-WRINKLE SERUM',
    price: 3150,
    description: 'Сыворотка против морщин с гиалуроновой кислотой',
    available: true,
    options: [
      {
        title: 'Бренд',
        text: 'Haruharu WONDER',
      },
      {
        title: 'Объем',
        text: '90 г',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Скажем “нет” морщинкам и признакам преждевременного старения. Сыворотка глубоко увлажняет эпидермис, придает сияние, ускоряет регенерацию клеток и поддерживает водный баланс.</p><p>Активные компоненты:</p><p>Гиалуроновая кислота — восполняет потерю влаги и фиксирует ее в глубоких слоях эпидермиса. Сыворотка с гиалуроновой кислотой делает лицо упругим и подтянутым, способствует регенерации поврежденных клеток.</p><p>Экстракт черного риса — интенсивно питает и увлажняет, осветляет, выравнивает тон кожи, делает кожу гладкой и шелковистой.</p><p>Масло лаванды — успокаивает, борется с микробами, оказывает антивозрастной эффект. Увлажняющая сыворотка улучшает микроциркуляцию в клетках и снимает стресс.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'D.I. WATER, Glycerin, Lecithin, Caprylic/Capric Triglyceride, Camellia Seed Oil, Hydrogenated Lecithin, Helianthus Annuus (Sunflower) Seed Oil, Oryza Sativa (Rice) Extract, Phyllostachys Pubescens Shoot Bark Extract, Aspergillus Ferment, Hyaluronic acid, Panax Ginseng Root Extract, Cyclodextrin, Sclerotium gum, C12-16 Alcohols, Palmitic Acid, Alteromonas ferment extract, Usnea Barbata (Lichen) Extract, Zanthoxylum Piperitum Fruit Extract, Pulsatilla Koreana Extract, beta-glucan, 1,2-Hexanediol, Glycine Soja (soybean) Sterois, Behenyl Alcohol, Simmondsia Chinensis (Jojoba) Seed Oil, Glyceryl Stearate, Sodium phytate, Tocopherol, Adenosine, Phenethyl Alcohol, Butylene Glycol, Alcohol, Lavandula Angustifolia (Lavender) Oil',
      },
    ],
  },
  '9ac14ffa-db29-4279-89a5-462a70b1ba9b': {
    img: 'reclar',
    name: 'RECLAR GALVANIC WATER PEELER SILVER',
    price: 10990,
    description: 'Многофункциональный корейский бьюти-гаджет 3в1',
    available: true,
    options: [
      {
        title: 'Бренд',
        text: 'Reclar',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Домашний уход на уровне процедур в кабинете косметолога? Уникальный корейский гаджет Reclar Galvanic Water Peeler Silver навсегда изменит вашу бьюти-рутину!</p><p>Всего за 15 минут в день, используя бьюти-гаджет RELCAR, вы сможете очистить поры от черных точек и других загрязнений, добьётесь более глубокого проникновения активных компонентов сыворотки в кожу, улучшите ее тон, придадите коже упругость и здоровый вид!</p><p>Это уникальный корейский бьюти-гаджет, который совмещает в себе сразу 3 эффективных функции для ухода за кожей лица: ультразвуковой пилинг, гальванотерапию и светотерапию</p>',
      },
    ],
  },
  '40f89e69-67e9-4001-a0da-655a8c72f348': {
    img: 'banila',
    name: 'BANILA CO DEAR HYDRATION BEST TRIO KIT',
    price: 780,
    description: 'Набор миниатюр для увлажнения',
    available: true,
    options: [
      {
        title: 'Бренд',
        text: 'Banila&Сo',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Dear Hydration Toner — тонер с экстрактами мяты, базилика, лотоса и бамбука. Обладает выраженным противовоспалительным, увлажняющим и питательным эффектом.</p><p>Dear Hydration Boosting Cream — интенсивное увлажнение и защитное покрытие, удерживающее влагу в глубоких слоях кожи. Корейский крем для лица выравнивает тон и подходит, как основа для макияжа.</p><p>Dear Hydration Intense Essence — содержит увлажняющую формулу, делающую кожу мягкой, эластичной и здоровой.</p><p>Активные компоненты:</p><p>Перечная мята — освежает и тонизирует, заживляет микротрещинки, разглаживает мелкие морщинки, улучшает цвет лица и устраняет первые признаки старения.</p><p>Экстракт базилика — мощный антиоксидант и антисептик. Успокаивает кожу, предупреждает старение и ускоряет регенерацию клеток.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          '<p>Dear Essence<br/>purified water, methylpropandium, glycerin, dipropylenglicol, 1,2-hexandi Eul, caraginan extract, sugarcane extract, lotus flower water, prickly bamboo sap, yeast extract, citrus sprout extract, nimbasyl leaf extract, peppermint extract, trilogylose, ponteol</p><p>Dear Toner<br/>purified water, propaneul, pentylene glycol, metamorphic alcole, bis-fiji-18me chiletelimethylsilan, glycerine, 1,2-hexandiul, apple water, lotus water, sodium-ha yalurunate, prickly bamboo sap, yeast extract, citrus sprout, spiny leaf extract, oleba-sil leaf extract</p><p>Dear Cream<br/>purified water, lotus water, glycerin, butylene glycol, dimecorn, cyclopenta siloxic acid, octylodecylistate, cyclohexyloxic acid, cetylpyge/fiji-10/1 dimeccon, hydrogenated poly (C6-14 olefin), 1,2-hexandeul, Indian aliment, Indian alum, Indian alum, Indian alum, alum. propylene glycol, ethyl hexylglycerin, zanthanum, disodium edithi, phenoxyethan, flavour, rimonen, citronelol, rinalul, gerani</p>',
      },
    ],
  },
  '5226c6a6-d2fc-4e24-89e1-1b20127649d9': {
    img: 'jmsolution',
    name: 'JM SOLUTION MARINE LUMINOUS PEARL MOISTURE EYE CREAM ALL FACE',
    price: 910,
    description:
      'Многофункциональный крем для глаз и лица с водорослями и жумчугом',
    available: false,
    options: [
      {
        title: 'Бренд',
        text: 'JM solution',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Крем для глаз и лица JM Solution Marine Luminous Pearl Moisture Eye Cream All Face на основе морской воды с экстрактом жемчужины абалона, морским коллагеном, экстрактами 7 морских водорослей, муцином слизи улитки, комплексом гиалуроновых кислот и керамидами.</p><p>Увлажняет и разглаживает кожу, делает ее мягкой и наполняет энергией морских минералов, уменьшает глубину морщин. Способствует регенерации клеток и выработке природного коллагена. Погомает избежать трансэпидермальной потери влаги клетками кожи, борется с процессами старения и куперозом, повышает барьерные свойства эпидермиса к воздействию неблагоприятных факторов окружающей среды.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'Water, Glycerin, Butylene Glycol, Disostearyl Malate, Hydrogenated Poly (C6-14 Olefin), Pentaerythrityl Tetraethylhexanoate, 1,2-Hexanediol, Caprylic/Capric Triglyceride, Ethylhexyl Palmitate, Niacinamide, Cyclopentasiloxane, Pentylene Glycol, Snail Secretion Filtrate, SeaWater (100ppm), Litchi Chinensis Pericarp Extract, Pueraria Lobata Root Extract, Pearl Extract (3ppm), Aesculus Hippocastanum (Horse Chestnut) Seed Extract, Hydrolyzed Collagen, Hyaluronic Acid, Codium Tomentosum Extract, Enteromorpha Compressa Extract, Gelidium Cartilagineum Extract, Laminaria Japonica Extract, Salicornia Herbacea Extract, Spirulina Platensis Extract, Undania Pinnatifida Extract, Hydrolyzed Hyaluronic Acid, Dimethicone, PEG-150 Distearate, Polyglyceryl-3 Methyiglucose Distearate, Glyceryl Stearate, Hydrogenated Olive Oil Lauryl Esters, Cyclohexasiloxane, Hydrogenated Lecithin, Miristyl Mynstate, Copernicia Cerifera (Carnauba) Wax, Hydroxyethyl Acrylate/Sodium Acryloyldimethyl Taurate Copolymer, Behenic Acid, Caprylyl Glycol, Stearic Acid, Ethylhexylglycerin, Tocopherol, Acetate, Adenosine, Myristic Acid, Panthenol, Propylene Glycol, Propanediol, Betaine, Polyacrylate-13, Mannitol, Polyisobutene, Hydrolyzed Glycosaminoglycans, Ammonium Glycymhizate, Ferulic Acid, Polysorbate 20, Sorbitan Isostearate, Beta-Glucan, Sclerotium Gum, Sodium Hyaluronate, Astaxanthin, Fullerenes, Polyglyceryl-10 Diisostearate, Hydroxypropyltrimonium Hyaluronate, Zinc Gluconate, Hydrolyzed Conchiolin Protein, PVP, Ceramide NP, Phospholipids, Phytosphingosine, Phytosterols, Tropolone, Cetearyl Alcohol, Stearyl Alcohol, Behenyl Alcohol, Xanthan Gum, Caffeine, Disodum EDTA, Phenoxyethanol, Chlorphenesin, Potassium Sorbate, Sodium Benzoate, Fragrance, Blue 1 (CI 42090)',
      },
    ],
  },
  '68d6120f-f60c-4764-a98e-de14be769d57': {
    img: 'missha',
    name: 'MISSHA PEARL IN LOVE GLOSS (PK/CALL YOU MINE)',
    price: 1215,
    description: 'Мерцающий блеск для губ',
    available: false,
    options: [
      {
        title: 'Бренд',
        text: 'Missha',
      },
      {
        title: 'Объем',
        text: '5,3 мл',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Яркие, сочные губы без липкого эффекта и жирной плёнки? Теперь это возможно – с блеском для губ Missha Pearl In Love Gloss от корейской марки Missha.</p><p>Россыпь сияющих, словно бриллианты, частичек перламутра придаёт губам дополнительный объём и делает их более привлекательными. Масло шиповника увлажняет, питает и смягчает нежную и тонкую кожу губ, а витамин Е (токоферол) насыщает клетки эпидермиса антиоксидантами и создаёт защитное покрытие, которое минимизирует негативное влияние факторов окружающей среды (пыль, выхлопные газы и ультрафиолетовое излучение).</p><p>Удобный слегка изогнутый спонж-аппликатор помогает точно дозировать средство и безупречно прокрашивает уголки губ и «лук Амура».</p><p>Способ применения: Нанесите средство на кожу губ с помощью аппликатора.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'вода, бутиленгликоль, етилгексилпальмитат, глицерин, пэг-60, гигироване уксусное масло, желтое 6 (ci 15985), пентиленгликоль, стевиозид, полиакрилат, кросполимер-6, аммоний, акрилоилдиметилтаурат / п.п., сополимер, феноксиэтанол, аромат, каприлилгликоль, 1,2 -гександиолl, етилгексилглицерин, пропандиол, red 33 (ci 17200), т-бутиловый спирт, экстракт фруктового льна (аниса), масло ячменя, масло абрикоса, семена огурцов, подсолнечное масло, экстракт меда, токоферол.',
      },
    ],
  },
  '6d09a5b1-22e2-4e38-bfa9-85db721e344c': {
    img: 'lador',
    name: 'LADOR WONDER BUBBLE SHAMPOO 250 ML',
    price: 790,
    description: 'Шампунь для объема и глубокого увлажнения волос',
    available: false,
    options: [
      {
        title: 'Бренд',
        text: 'Lador',
      },
      {
        title: 'Объем',
        text: '250 мл',
      },
    ],
    tabs: [
      {
        id: 'specification',
        title: 'Описание',
        content:
          '<p>Шампунь отлично очищает волосы и кожу головы от кожного жира и других загрязнений. А также оказывает увлажняющее и питательное действие, делает волосы мягкими и блестящими, создавая естественный прикорневой объём.</p><p>Кроме того, средство защищает волосы от агрессивного воздействия окружающей среды, обеспечивает надежную защиту цвета для окрашенных волос, облегчает расчесывание и укладку.</p><p>Шампунь образует обильную пену и обладает лёгким цветочным ароматом.</p><h3>Способ применения.</h3><p>Небольшое количество шампуня нанесите на влажные корни и кожу головы, помассируйте до образования пены, а затем смойте тёплой водой. При необходимости повторите процедуру.</p>',
      },
      {
        id: 'structure',
        title: 'Состав',
        content:
          'Purified water, sodium laureth sulfate, ammonium lauryl sulfate, cocamidopropyl betaine, glycerin, disodium coco ampodiete acetate, dimethicone, glycgol diestearate, cocamide emei, polyquaternium-7, di Propylene Glycol, Polyquaternium-10, Guarhydroxy Propyl Trimonium Chloride, Hydroxyethyl Urea, Sodium Hyaluronate, Hydrolyzed Keratin, Henna Extract, Pea Peptide Acetyl Nucleopeptide-8, Copper Tripeptide 1, palmitoyl penta peptide 4, tripub tide-1 hexapeptide.9, palmitoyl tripub tide-1, polyglyceryl-10 laurate, citric acid, C12-15 pares-3, apanoteche sacrum Polysaccharide Hydroxy Acetophenone, fragrance',
      },
    ],
  },
};

export const {actions, reducer} = createSlice({
  name: 'goods',
  initialState,
  reducers
});

export const { sortBy, filterAvailable } = actions;

export default reducer;
