import { initialState } from "./slice";

const reducers = {
  changeSort: (state, {payload: type}) => {
    if (type === '') {
      state = initialState;
    } else {
      state[type] = !state[type];
    }

    return state;
  },
  setActiveSort: (state, {payload: type}) => {
    state.activeSort = type;

    return state;
  },
};

export default reducers;
