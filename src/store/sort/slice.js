import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

export const initialState = {
  price: true,
  name: true,
  activeSort: '',
  available: false,
};

export const { actions, reducer } = createSlice({
  name: 'sort',
  initialState,
  reducers
});

export const { changeSort, setActiveSort } = actions;

export default reducer;
