const selectSort = (state) => state.sort;

export const selectActiveSort = (state) => state.sort.activeSort;

export default selectSort;
