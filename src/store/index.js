import logger from 'redux-logger';
import { configureStore } from '@reduxjs/toolkit';
import goods from './goods';
import sort from './sort';
import cart from './cart';
import order from './order';
import favorites from './favorites';
import compare from './compare';

const reducer = {
  goods,
  sort,
  cart,
  order,
  favorites,
  compare,
};

export default configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});
