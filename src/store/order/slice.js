import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

const initialState = {
  name: '',
  surname: '',
  phone: '',
  email: '',
  comment: '',
  cardNumber: '',
  cardName: '',
  cardMonth: '',
  cardYear: '',
  cardCvv: '',
  address: '',
  addressCoords: '',
};

const { actions, reducer } = createSlice({
  name: 'order',
  initialState,
  reducers
});

export const { setOrderInfo } = actions;

export default reducer;
