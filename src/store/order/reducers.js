const reducers = {
  setOrderInfo: (state, { payload }) => {
    const { name, value } = payload;
    state[name] = value;
    return state;
  },
};

export default reducers;
