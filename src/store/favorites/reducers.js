const reducers = {
  addToFavorites: (state, { payload: itemId }) => {
    const index = state.findIndex((e) => e === itemId);

    if (index === -1) {
      state.push(itemId);
    } else {
      state[index] = itemId;
    }

    return state;
  },
  removeFromFavorites: (state, { payload: itemId }) => {
    state = state.filter((favItem) => favItem !== itemId);

    return state;
  },
};

export default reducers;
