import { createSlice } from "@reduxjs/toolkit";
import reducers from "./reducers";

export const {actions, reducer} = createSlice(
  {
    name: 'favorites',
    initialState: [],
    reducers
  }
)

export const { addToFavorites, removeFromFavorites } = actions;

export default reducer;