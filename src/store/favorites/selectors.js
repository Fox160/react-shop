import { createSelector } from "reselect";

const selectFavorites = (state) => state.favorites;

export const selectFavoritesItem = createSelector(
  (state) => state.favorites,
  (_, id) => id,
  (favorites, id) => favorites.filter((favItem) => favItem === id)[0]
);

export default selectFavorites;
