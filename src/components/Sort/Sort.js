import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { filterAvailable, sortBy } from '../../store/goods/slice';
import { changeSort, setActiveSort } from '../../store/sort/slice';
import selectSort, { selectActiveSort } from '../../store/sort/selectors';

import arrow from '../../assets/img/arrow.svg';

const sortClasses = { asc: -1, desc: 1 };

const Sort = () => {
  const dispatch = useDispatch();
  const sort = useSelector(selectSort);
  const activeSort = useSelector(selectActiveSort);

  const getSortDirection = (type) => {
    return sort[type] ? 1 : -1;
  };

  const getSortClass = () => {
    if (activeSort === '') {
      return '';
    }

    return Object.keys(sortClasses).find(
      (key) => sortClasses[key] === getSortDirection(activeSort)
    );
  };

  const handleSort = (type) => {
    dispatch(changeSort(type));

    let direction = getSortDirection(type);
    dispatch(sortBy({ type, direction }));
  };

  const handleChange = (e) => {
    dispatch(filterAvailable(e.target.checked));
    dispatch(changeSort(e.target.name));
  };

  const handleSortChange = (type) => {
    handleSort(type);
    dispatch(setActiveSort(type));
  };

  const handleReset = () => {
    handleSort('');
    dispatch(setActiveSort(''));
  };

  return (
    <div className="sort">
      <button
        onClick={() => handleSortChange('price')}
        className={`sort__btn ${
          activeSort === 'price' && getSortClass(activeSort)
        }`}
      >
        Сортировать по цене
        <img src={arrow} className="sort__icon" />
      </button>
      <button
        onClick={() => handleSortChange('name')}
        className={`sort__btn ${
          activeSort === 'name' && getSortClass(activeSort)
        }`}
      >
        Сортировать по названию
        <img src={arrow} className="sort__icon" />
      </button>
      <input
        className="sort__checkbox"
        id="filter"
        type="checkbox"
        name="available"
        checked={sort.available || false}
        onChange={(e) => handleChange(e)}
      />
      <label htmlFor="filter">Только в наличии</label>

      <button
        className="btn btn--link sort__reset"
        onClick={() => handleReset()}
      >
        Сбросить
      </button>
    </div>
  );
};

export default Sort;
