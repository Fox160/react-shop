import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import sprite from '../../assets/img/sprite.svg';

const Header = () => {
  const { cart, favorites, compare } = useSelector(
    (state) => state,
    shallowEqual
  );

  return (
    <header className="header">
      <div className="header__container">
        <Link to="/" className="header__nav">
          Главная
        </Link>
        <Link to="/cart" className="header__nav">
          Корзина
          {Object.keys(cart).length > 0 && (
            <div className="header__count">
              <span>{Object.keys(cart).length}</span>
            </div>
          )}
        </Link>
        <Link
          to="/favorites"
          className="header__nav header__nav--icon header__nav--right"
        >
          <svg>
            <use href={sprite + '#fav'} />
          </svg>
          {favorites.length > 0 && (
            <div className="header__count">
              <span>{favorites.length}</span>
            </div>
          )}
        </Link>
        <Link to="/compare" className="header__nav header__nav--icon">
          <svg>
            <use href={sprite + '#compare'} />
          </svg>
          {compare.length > 0 && (
            <div className="header__count">
              <span>{compare.length}</span>
            </div>
          )}
        </Link>
      </div>
    </header>
  );
};

export default Header;
