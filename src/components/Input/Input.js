import React from 'react';

const Input = ({
  wrapperClass,
  label,
  className,
  id,
  name,
  type = 'text',
  placeholder,
  value,
  onChange,
  children,
  onFocus,
  onBlur
}) => {
  return (
    <div className={wrapperClass}>
      <label htmlFor={id}>{label}</label>
      {children || (
        <input
          className={className}
          id={id}
          name={name}
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
        />
      )}
    </div>
  );
};

export default Input;
