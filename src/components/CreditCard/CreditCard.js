import React, { useEffect, useState } from 'react';

import CreditCardNumbers from './CreditCardNumbers';
import CreditCardHolder from './CreditCardHolder';
import CreditCardDate from './CreditCardDate';

import getCardType from '../../helpers/getCardType';

import visa from '../../assets/img/visa_logo.png';
import mastercard from '../../assets/img/mc_logo.png';
import cardCover from '../../assets/img/card-cover.jpeg';
import cardChip from '../../assets/img/chip.png';

const cardTypeToLogo = {
  mastercard,
  visa,
};

const CreditCard = ({
  cardType: propsCardType,
  styleName,
  cardNumber,
  cardName,
  isCardFlipped,
  cardYear,
  cardMonth,
  cardCvv,
}) => {
  const cardMask = '#### #### #### ####';
  const [cardType, setCardType] = useState(propsCardType || 'visa');

  useEffect(() => {
    setCardType(getCardType(cardNumber));
  }, [cardNumber]);

  return (
    <div
      className={
        `credit-card ${styleName} ` +
        (isCardFlipped ? 'credit-card--active' : '')
      }
    >
      <div className="credit-card__item credit-card__item--front">
        <div className="credit-card__cover">
          <img src={cardCover} className="credit-card__bg" />
        </div>
        <div className="credit-card__wrapper">
          <div className="credit-card__top">
            <img src={cardChip} className="credit-card__chip" />

            <div className="credit-card__logo">
              <img className="logo" src={cardTypeToLogo[cardType]} width="60" />
            </div>
          </div>

          <label className="credit-card__number">
            {[...cardMask].map((n, index) => (
              <span key={index}>
                <CreditCardNumbers
                  n={n}
                  index={index}
                  cardNumber={cardNumber}
                />
              </span>
            ))}
          </label>

          <div className="credit-card__info">
            <CreditCardHolder cardName={cardName} />

            <CreditCardDate cardMonth={cardMonth} cardYear={cardYear} />
          </div>
        </div>
      </div>

      <div className="credit-card__item credit-card__item--back">
        <div className="credit-card__cover">
          <img src={cardCover} className="credit-card__bg" />
        </div>
        <div className="credit-card__band"></div>
        <div className="credit-card__cvv">
          <div className="credit-card__cvvTitle">CVV</div>
          <div className="credit-card__cvvBand">
            {[...cardCvv].map((n, index) => (
              <span key={index}>*</span>
            ))}
          </div>
          <div className="credit-card__type">
            {cardTypeToLogo[cardType] ? (
              <img
                src={cardTypeToLogo[cardType]}
                width="60"
                className="credit-card__typeImg"
              />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreditCard;
