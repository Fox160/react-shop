import React from 'react';
import { CSSTransition, SwitchTransition } from 'react-transition-group';

const CreditCardDate = ({ cardMonth, cardYear }) => (
  <div className="credit-card__date">
    <div className="credit-card__dateTitle">Срок действия</div>
    <label htmlFor="cardMonth" className="credit-card__dateItem">
      <SwitchTransition>
        <CSSTransition
          classNames="slide-fade-up"
          timeout={200}
          key={cardMonth}
          addEndListener={(node, done) => {
            node.addEventListener('transitionend', done, false);
          }}
        >
          <span key={cardMonth ? "cardMonth" : "2"}>{cardMonth || "MM"}</span>
        </CSSTransition>
      </SwitchTransition>
    </label>
    /
    <label htmlFor="cardYear" className="credit-card__dateItem">
      <SwitchTransition>
        <CSSTransition
          classNames="slide-fade-up"
          timeout={200}
          key={cardYear}
          addEndListener={(node, done) => {
            node.addEventListener('transitionend', done, false);
          }}
        >
          <span key={cardYear ? "cardYear" : "2"}>{cardYear ? String(cardYear).slice(2, 4) : "YY"}</span>
        </CSSTransition>
      </SwitchTransition>
    </label>
  </div>
);

export default CreditCardDate;
