import React from 'react';
import { CSSTransition, SwitchTransition } from 'react-transition-group';

const CreditCardNumbers = ({ index, cardNumber, n }) => {
  const isNumber = cardNumber.length > index;
  let numberBlock = null;

  if (index > 4 && index < 15 && cardNumber.length > index && n.trim() !== '') {
    numberBlock = <div className="credit-card__numberItem">*</div>;
  } else {
    numberBlock = (
      <div
        className={
          'credit-card__numberItem ' + (n.trim() === '' ? '-active' : '')
        }
        key={index + isNumber ? 0 : 1}
      >
        {isNumber ? cardNumber[index] : n}
      </div>
    );
  }

  return (
    <SwitchTransition>
      <CSSTransition
        classNames="slide-fade-up"
        timeout={200}
        key={`${index}-${cardNumber[index]}`}
        addEndListener={(node, done) => {
          node.addEventListener('transitionend', done, false);
        }}
      >
        {numberBlock}
      </CSSTransition>
    </SwitchTransition>
  );
};

export default CreditCardNumbers;
