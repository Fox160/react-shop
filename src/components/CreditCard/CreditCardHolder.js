import React from 'react';
import {
  CSSTransition,
  TransitionGroup,
  SwitchTransition,
} from 'react-transition-group';

const CreditCardHolder = ({ cardName }) => (
  <div className="credit-card__info_name">
    <div className="credit-card__dateTitle">Держатель карты</div>
    <SwitchTransition>
      <CSSTransition
        classNames="slide-fade-up"
        timeout={200}
        key={cardName.length ? '1' : '2'}
        addEndListener={(node, done) => {
          node.addEventListener('transitionend', done, false);
        }}
      >
        {cardName.length ? (
          <div className="credit-card__name" key="1">
            <TransitionGroup>
              {[...cardName.replace(/\s\s+/g, ' ')].map((n, index) => (
                <CSSTransition
                  classNames="slide-fade-up"
                  timeout={200}
                  key={index}
                >
                  <span className="credit-card__nameItem">{n}</span>
                </CSSTransition>
              ))}
            </TransitionGroup>
          </div>
        ) : (
          <div className="credit-card__name" key="2">
            Фамилия Имя
          </div>
        )}
      </CSSTransition>
    </SwitchTransition>
  </div>
);

export default CreditCardHolder;
