import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { selectGoodsItem } from '../../store/goods/selectors';

import GoodsDetailProduct from './GoodsDetailProduct';

const GoodsDetail = (props) => {
  const { id } = props.match.params;
  const goodsItem = useSelector((state) => selectGoodsItem(state, id));
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className="goods-detail goods-item--detail container">
      <GoodsDetailProduct product={goodsItem} id={id} />

      {goodsItem.tabs && (
        <div className="goods-detail__tabs">
          {goodsItem.tabs.map((tab, index) => (
            <button
              type="button"
              className={`goods-detail__tab ${
                activeTab === index ? 'active' : ''
              }`}
              key={tab.id}
              onClick={() => setActiveTab(index)}
            >
              {tab.title}
            </button>
          ))}
          <div
            className="goods-detail__tabs-content"
            dangerouslySetInnerHTML={{
              __html: goodsItem.tabs[activeTab].content,
            }}
          ></div>
        </div>
      )}
    </div>
  );
};

export default GoodsDetail;
