import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { addToCart } from '../../store/cart/slice';
import {
  addToFavorites,
  removeFromFavorites,
} from '../../store/favorites/slice';
import { selectFavoritesItem } from '../../store/favorites/selectors';
import { addToCompare, removeFromCompare } from '../../store/compare/slice';
import { selectCompareItem } from '../../store/compare/selectors';

import RenderInfo from '../GoodsItem/RenderInfo';

import valmona from '../../assets/img/valmona.jpg';
import holika from '../../assets/img/holika.jpg';
import ciracle from '../../assets/img/ciracle.jpg';
import haruharu from '../../assets/img/haruharu.jpg';
import reclar from '../../assets/img/reclar.jpg';
import banila from '../../assets/img/banila.jpg';
import jmsolution from '../../assets/img/jmsolution.jpg';
import missha from '../../assets/img/missha.jpg';
import lador from '../../assets/img/lador.jpg';

const images = {
  valmona,
  holika,
  ciracle,
  haruharu,
  reclar,
  banila,
  jmsolution,
  missha,
  lador,
};

const GoodsDetailProduct = ({ product, id }) => {
  const dispatch = useDispatch();
  let goodsItemFav = useSelector((state) => selectFavoritesItem(state, id));
  let goodsItemCompare = useSelector((state) => selectCompareItem(state, id));

  const handleClickFav = () => {
    if (goodsItemFav) {
      dispatch(removeFromFavorites(id));
    } else {
      dispatch(addToFavorites(id));
    }
  };

  const handleClickCompare = () => {
    if (goodsItemCompare) {
      dispatch(removeFromCompare(id));
    } else {
      dispatch(addToCompare(id));
    }
  };

  return (
    <div className="goods-detail__product">
      <div className="goods-detail__product-col">
        <img src={images[product.img]} className="goods-item__img" />
      </div>
      <div className="goods-detail__product-col">
        <h1 className="goods-detail__title">{product.name}</h1>
        <div className="goods-detail__description">{product.description}</div>

        {product.options &&
          product.options.map((optionItem, index) => (
            <div className="goods-detail__option" key={index}>
              <div className="goods-detail__option-title">
                {optionItem.title}
              </div>
              <div className="goods-detail__option-text">{optionItem.text}</div>
            </div>
          ))}

        <div className="goods-detail__actions">
          {product.price && (
            <p className="goods-item__price">{product.price} ₽</p>
          )}
          {product.available === false ? (
            <div className="goods-item__text-out goods-item__btn">
              Нет в наличии
            </div>
          ) : (
            <div className="goods-detail__actions-row">
              <RenderInfo
                favBtn
                isGoodsFav={goodsItemFav}
                handleClickFav={() => handleClickFav()}
                handleClick={() => dispatch(addToCart(id))}
                buttonText="Добавить в корзину"
                compareBtn
                isGoodsCompare={goodsItemCompare}
                handleClickCompare={() => handleClickCompare()}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default GoodsDetailProduct;
