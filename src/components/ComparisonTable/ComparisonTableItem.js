import React from 'react';
import { useDispatch } from 'react-redux';

import { removeFromCompare } from '../../store/compare/slice';

import GoodsItem from '../GoodsItem/GoodsItem';

const ComparisonTableItem = ({ options, id, name, img, price }) => {
  const dispatch = useDispatch();

  const sizeOption = options.filter(({ title }) => title === 'Объем')[0];
  const brandOption = options.filter(({ title }) => title === 'Бренд')[0];

  return (
    <ul className="compare-table__row">
      <>
        <li>
          <GoodsItem
            styleName="compare-table__goods"
            id={id}
            name={name}
            img={img}
            handleClick={() => dispatch(removeFromCompare(id))}
            removeBtn={true}
          />
        </li>
        <li>{sizeOption ? brandOption.text : '-'}</li>
        <li>{price ? `${price} ₽` : '-'}</li>
        <li>{brandOption ? brandOption.text : '-'}</li>
      </>
    </ul>
  );
};

export default ComparisonTableItem;
