import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import selectCompare from '../../store/compare/selectors';
import selectGoods from '../../store/goods/selectors';

import ComparisonTableItem from './ComparisonTableItem';

const ComparisonTable = () => {
  const comparesStore = useSelector(selectCompare);
  const goods = useSelector(selectGoods);

  return (
    <div
      className={`compare-table container ${
        !(comparesStore.length > 0) ? 'compare-table--empty' : ''
      }`}
    >
      <div className="compare-table__content">
        {comparesStore.length > 0 ? (
          <>
            <ul className="compare-table__row compare-table__row--head">
              <li className="compare-table__legend">Товар</li>
              <li className="compare-table__legend">Объем</li>
              <li className="compare-table__legend">Цена</li>
              <li className="compare-table__legend">Бренд</li>
            </ul>
            {comparesStore.map((id) => (
              <ComparisonTableItem {...goods[id]} key={id} id={id} />
            ))}
          </>
        ) : (
          <>
            <p className="compare-table__title">
              Нет товаров для сравения.{' '}
              <Link to="/" className="link">
                Выберите
              </Link>{' '}
              товары прямо сейчас!
            </p>
          </>
        )}
      </div>
    </div>
  );
};

export default ComparisonTable;
