import React from 'react';

import sprite from '../../assets/img/sprite.svg';

const CompareBtn = ({ onClick, isActive, className }) => (
  <button
    className={`btn btn--link btn--svg ${className} ${
      isActive ? 'active' : ''
    }`}
    onClick={onClick}
  >
    <svg>
      <use href={sprite + '#compare'} />
    </svg>
  </button>
);

export default CompareBtn;
