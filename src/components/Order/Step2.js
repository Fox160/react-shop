import React, { useState } from 'react';
import InputMask from 'react-input-mask';

import CreditCard from '../CreditCard/CreditCard';
import Input from '../Input/Input';

const Step2 = ({
  cardNumber,
  cardName,
  cardYear,
  cardMonth,
  cardCvv,
  handleChange,
}) => {
  const minCardYear = new Date().getFullYear();
  const [isCardFlipped, setIsCardFlipped] = useState(false);

  const minCardMonth = () => {
    if (cardYear === minCardYear || cardYear === '') {
      return ('0' + (new Date().getMonth() + 1)).slice(-2);
    }

    return '01';
  };

  return (
    <div className="order__form-group">
      <h2 className="order__title">Оплата</h2>
      <CreditCard
        cardNumber={cardNumber}
        cardName={cardName}
        cardYear={cardYear}
        cardMonth={cardMonth}
        cardCvv={cardCvv}
        isCardFlipped={isCardFlipped}
        styleName="order__credit-card"
      />

      <Input
        wrapperClass="order__input-group"
        label="Номер карты"
        id="cardNumber"
      >
        <InputMask
          mask="9999 9999 9999 9999"
          maskChar="#"
          className="order__input"
          id="cardNumber"
          name="cardNumber"
          placeholder="Введите номер карты"
          value={cardNumber}
          onChange={(event) => {
            handleChange(event);
          }}
        />
      </Input>

      <Input
        wrapperClass="order__input-group"
        label="Держатель карты"
        className="order__input"
        id="cardName"
        name="cardName"
        placeholder="Фамилия и имя"
        value={cardName}
        onChange={(event) => {
          handleChange(event);
        }}
      />

      <div className="order__row">
        <div className="order__col">
          <div className="order__group order__input-group">
            <label htmlFor="cardMonth" className="card-input__label">
              Срок действия
            </label>
            <select
              className="order__select order__input order__col-item"
              id="cardMonth"
              value={cardMonth < minCardMonth() ? '' : cardMonth}
              name="cardMonth"
              onChange={(event) => {
                handleChange(event);
              }}
            >
              <option value="" disabled>
                Месяц
              </option>
              {[...Array(12)].map((n, index) => {
                let monthIndex = index + 1;

                return (
                  <option
                    value={monthIndex < 10 ? '0' + monthIndex : monthIndex}
                    disabled={
                      (monthIndex < 10 ? '0' + monthIndex : monthIndex + '') <
                      minCardMonth()
                    }
                    key={`month-${monthIndex}`}
                  >
                    {index < 9 ? '0' + monthIndex : monthIndex}
                  </option>
                );
              })}
            </select>
            <select
              className="order__select order__input order__col-item"
              id="cardYear"
              value={cardYear}
              name="cardYear"
              onChange={(event) => {
                handleChange(event);
              }}
            >
              <option value="" disabled>
                Год
              </option>
              {[...Array(12)].map((n, index) => (
                <option value={index + minCardYear} key={`year-${index}`}>
                  {index + minCardYear}
                </option>
              ))}
            </select>
          </div>
        </div>

        <Input
          wrapperClass="order__input-group"
          label="CVV"
          className="order__input"
          id="cardCvv"
          name="cardCvv"
          placeholder="CVV"
          value={cardCvv}
          onChange={(event) => {
            handleChange(event);
          }}
          onFocus={() => setIsCardFlipped(true)}
          onBlur={() => setIsCardFlipped(false)}
        />
      </div>
    </div>
  );
};

export default Step2;
