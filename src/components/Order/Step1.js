import React from 'react';
import InputMask from 'react-input-mask';

import Input from '../Input/Input';

const Step1 = ({ name, handleChange, surname, phone, email, comment }) => {
  return (
    <div className="order__form-group">
      <h2 className="order__title">Контакты</h2>

      <Input
        wrapperClass="order__input-group"
        label="Имя"
        className="order__input"
        id="name"
        name="name"
        placeholder="Введите имя"
        value={name}
        onChange={handleChange}
      />

      <Input
        wrapperClass="order__input-group"
        label="Фамилия"
        className="order__input"
        id="surname"
        name="surname"
        placeholder="Введите фамилию"
        value={surname}
        onChange={handleChange}
      />

      <Input wrapperClass="order__input-group" label="Телефон" id="surname">
        <InputMask
          mask="+7 (999) 999-99-99"
          className="order__input"
          id="phone"
          name="phone"
          placeholder="Введите номер телефона"
          value={phone}
          onChange={handleChange}
        />
      </Input>

      <Input
        wrapperClass="order__input-group"
        label="Email"
        className="order__input"
        id="email"
        name="email"
        type="email"
        placeholder="Введите email"
        value={email}
        onChange={handleChange}
      />

      <Input
        wrapperClass="order__input-group"
        label="Комментарий"
        className="order__input"
        id="comment"
        name="comment"
        placeholder="Дополнительный комментарий к заказу"
        value={comment}
        onChange={handleChange}
      />
    </div>
  );
};

export default Step1;
