import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { setOrderInfo } from '../../store/order/slice';
import selectOrder from '../../store/order/selectors';
import { clearCart } from '../../store/cart/slice';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import OrderButtons from './OrderButtons';

const requiredFields = [
  ['name', 'surname', 'phone'],
  ['cardNumber', 'cardName', 'cardMonth', 'cardYear', 'cardCvv'],
  ['address'],
];

const OrderForm = () => {
  const [currentStep, setCurrentStep] = useState(1);
  const form = useSelector(selectOrder);
  const dispatch = useDispatch();
  const history = useHistory();

  const _next = () => {
    const currentStepFields = requiredFields[currentStep - 1];
    let isValidStep = true;

    if (currentStepFields) {
      currentStepFields.forEach((fieldName) => {
        const fieldEl = document.querySelector(
          `.order__input[name="${fieldName}"]`
        );

        if (form[fieldName] === '') {
          if (fieldEl) fieldEl.classList.add('error');
          isValidStep = false;

          return;
        }

        if (fieldEl) fieldEl.classList.remove('error');
      });
    }

    if (isValidStep) {
      setCurrentStep(currentStep >= 2 ? 3 : currentStep + 1);
    }
  };

  const _prev = () => {
    setCurrentStep(currentStep <= 1 ? 1 : currentStep - 1);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;

    if (event.target instanceof Element && requiredFields.length > 0) {
      const targetValidator = requiredFields[currentStep - 1].filter(
        (str) => str === name
      )[0];

      if (targetValidator && value === '') {
        event.target.classList.add('error');
      } else {
        event.target.classList.remove('error');
      }
    }

    dispatch(setOrderInfo({ name, value }));
  };

  // Сабмит формы
  const handleSubmit = (event) => {
    event.preventDefault();
    history.push('/order-success');

    if (Object.values(form).some((value) => value !== '')) {
      dispatch(clearCart());
    }
  };

  return (
    <div className="order">
      <h1>Оформление заказа</h1>
      <form onSubmit={handleSubmit} className="order__form">
        {currentStep === 1 && (
          <Step1
            currentStep={currentStep}
            handleChange={handleChange}
            email={form.email}
            name={form.name}
            surname={form.surname}
            phone={form.phone}
            comment={form.comment}
            requiredFields={requiredFields[0]}
          />
        )}

        {currentStep === 2 && (
          <Step2
            currentStep={currentStep}
            handleChange={handleChange}
            cardNumber={form.cardNumber}
            cardName={form.cardName}
            cardMonth={form.cardMonth}
            cardYear={form.cardYear}
            cardCvv={form.cardCvv}
            requiredFields={requiredFields[0]}
          />
        )}

        {currentStep === 3 && (
          <Step3
            currentStep={currentStep}
            handleChange={handleChange}
            address={form.address}
            addressCoords={form.addressCoords}
            requiredFields={requiredFields[0]}
          />
        )}

        <OrderButtons currentStep={currentStep} _prev={_prev} _next={_next} />
      </form>
    </div>
  );
};

export default OrderForm;
