import React from 'react';
import { useSelector } from 'react-redux';

import selectOrder from '../../../store/order/selectors';

const titles = {
  name: 'Имя',
  surname: 'Фамилия',
  phone: 'Телефон',
  email: 'Email',
  comment: 'Комментарий',
  cardNumber: 'Номер карты',
  cardName: 'Держатель карты',
  cardMonth: 'Месяц',
  cardYear: 'Год',
  cardCvv: 'CVV',
  address: 'Адрес доставки',
};

const OrderSuccess = () => {
  const form = useSelector(selectOrder);
  const isFormFill = Object.values(form).some((value) => value !== '');

  return (
    <div className={`order-success ${!isFormFill && 'order-success--empty'}`}>
      {!isFormFill ? (
        <>
          <h1>Заказ не оформлен</h1>
          <p>
            Похоже, вы не ввели данные для оформления заказа.
            <br />
            Пожалуйста, вернитесь в корзину и повторите шаги.
          </p>
        </>
      ) : (
        <>
          <h1>Заказ успешно оформлен!</h1>
          {Object.entries(form).map(([key, value]) => {
            if (titles[key] && value !== '') {
              return (
                <div className="order-success__item" key={key}>
                  <div className="order-success__title">{titles[key]}</div>
                  <div className="order-success__text">{value}</div>
                </div>
              );
            }
          })}
        </>
      )}
    </div>
  );
};

export default OrderSuccess;
