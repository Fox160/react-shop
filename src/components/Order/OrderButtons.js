import React from 'react';

const OrderButtons = ({ currentStep, _prev, _next }) => (
  <div className="order__buttons">
    {currentStep !== 1 && (
      <button className="btn btn--secondary" type="button" onClick={_prev}>
        Назад
      </button>
    )}

    {currentStep < 3 && (
      <button className="btn float-right" type="button" onClick={_next}>
        Далее
      </button>
    )}

    {currentStep === 3 && (
      <button type="submit" className="btn btn-success btn-block">
        Оформить заказ
      </button>
    )}
  </div>
);

export default OrderButtons;
