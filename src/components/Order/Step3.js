import React, { useEffect, useState } from 'react';
import { Map, YMaps } from 'react-yandex-maps';

const mapState = {
  center: [55.75, 37.57],
  zoom: 9,
  controls: ['zoomControl'],
};

const Step3 = ({ address, addressCoords, handleChange }) => {
  const [inputValue, setInputValue] = useState(address);
  const [showMap, setShowMap] = useState(false);
  const [placemarkCoords, setPlacemarkCoords] = useState(addressCoords || null);

  const ymaps = React.useRef(null);
  const placemarkRef = React.useRef(null);
  const mapRef = React.useRef(null);

  useEffect(() => {
    setInputValue(address);
  }, [address]);

  useEffect(() => {
    handleChange({ target: { name: 'address', value: inputValue } });
  }, [inputValue]);

  const handleMapClick = (e) => {
    const coords = e.get('coords');

    addPlacemark(coords);
    geocode(coords);
  };

  const addPlacemark = (coords) => {
    mapRef.current.geoObjects.removeAll();
    placemarkRef.current = new ymaps.current.Placemark(coords);
    mapRef.current.geoObjects.add(placemarkRef.current);
    mapRef.current.setBounds(mapRef.current.geoObjects.getBounds());
    setPlacemarkCoords(coords);
    handleChange({ target: { name: 'addressCoords', value: coords } });
  };

  const geocode = (coords = null, queryStr = '') => {
    if (ymaps.current) {
      ymaps.current.geocode(coords || queryStr).then((res) => {
        if (coords) {
          setInputValue(res.geoObjects.get(0).getAddressLine());
        } else {
          addPlacemark(res.geoObjects.get(0).geometry.getCoordinates());
        }
      });
    }
  };

  return (
    <>
      <div className="order__form-group">
        <h2 className="order__title">Доставка</h2>
        <input
          type="hidden"
          name="addressCoords"
          value={placemarkCoords || ''}
        />

        <div className="order__row">
          <div className="order__col">
            <div className="order__group order__input-group order__group--col">
              <label htmlFor="address">Местоположение</label>
              <input
                className="order__input order__col-item"
                id="address"
                name="address"
                type="text"
                placeholder="Введите адрес"
                value={inputValue}
                onChange={(e) => {
                  handleChange(e);
                  geocode(null, e.target.value);
                }}
              />
              <button
                type="button"
                className="btn btn--link order__col-item order__col-item--link"
                onClick={() => setShowMap(!showMap)}
              >
                {!showMap ? 'Показать карту' : 'Скрыть карту'}
              </button>
            </div>
          </div>
        </div>
      </div>
      {showMap && (
        <YMaps query={{ apikey: '1b0726df-2a3a-4aa2-9331-fc0022dafd0f' }}>
          <Map
            className="order__map"
            instanceRef={mapRef}
            onLoad={(ympasInstance) => {
              ymaps.current = ympasInstance;

              if (placemarkCoords) {
                addPlacemark(placemarkCoords);
              } else if (inputValue) {
                geocode(null, inputValue);
              }
            }}
            onClick={handleMapClick}
            state={mapState}
            options={{ maxZoom: 18 }}
            modules={[
              'control.ZoomControl',
              'geolocation',
              'geocode',
              'Placemark',
            ]}
          />
        </YMaps>
      )}
    </>
  );
};

export default Step3;
