import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { addToCart } from '../../store/cart/slice';
import selectGoods from '../../store/goods/selectors';

import GoodsItem from '../GoodsItem/GoodsItem';
import Sort from '../Sort/Sort';

const GoodsList = () => {
  const goods = useSelector(selectGoods);
  const dispatch = useDispatch();

  let itemBlock = 'Нет товаров';

  if (Object.keys(goods).length > 0) {
    itemBlock = Object.keys(goods).map((key, value) => {
      const item = goods[key];

      return (
        <GoodsItem
          key={key}
          styleName="goods-list__item"
          id={key}
          {...item}
          handleClick={() => dispatch(addToCart(key))}
          buttonText="Добавить в корзину"
          favBtn
          compareBtn
        />
      );
    });
  }

  return (
    <div className="goods-list container">
      <Sort />
      <div
        className={`goods-list__wrapper ${
          goods.length === 0 ? 'goods-list__wrapper--empty' : ''
        }`}
      >
        {itemBlock}
      </div>
    </div>
  );
};

export default GoodsList;
