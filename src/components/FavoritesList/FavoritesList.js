import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { removeFromFavorites } from '../../store/favorites/slice';

import GoodsItem from '../GoodsItem/GoodsItem';

const FavoritesList = () => {
  const { favorites, goods } = useSelector((state) => state, shallowEqual);

  return (
    <div className={`favorites-list container ${!(favorites.length > 0) ? 'favorites-list--empty' : ''}`}>
      {favorites.length > 0 ? (
        <>
          <h1>Избранное</h1>
          <div className="goods-list favorites-list__list">
            <div className="goods-list__wrapper">
              {favorites.map((id) => {
                const goodsItem = goods[id];

                return (
                  <GoodsItem
                    {...goodsItem}
                    key={id}
                    id={id}
                    handleClickFav={() => dispatch(removeFromFavorites(goodsItem))}
                    styleName="favorites-list__item goods-item--small goods-list__item"
                    favBtn
                  />
                )
              })}
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="favorites-list__wrapper">
            <p className="favorites-list__title">В избранном пусто. <Link to="/" className="link">Выберите</Link> косметику прямо сейчас!</p>
          </div>
        </>
      )}
    </div>
  );
}

export default FavoritesList;