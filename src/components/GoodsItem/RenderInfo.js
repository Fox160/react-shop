import React from 'react';

import CompareBtn from '../CompareBtn/CompareBtn';
import FavBtn from '../FavBtn/FavBtn';

const RenderInfo = (props) => {
  return (
    <>
      {props.count && <p className="goods-item__count">{props.count} шт.</p>}
      {props.buttonText && (
        <button
          className="btn goods-item__btn"
          onClick={() => props.handleClick(props)}
        >
          {props.buttonText}
        </button>
      )}
      {!props.hideFav && (
        <FavBtn
          className="goods-detail__btn-icon"
          isActive={props.isGoodsFav}
          onClick={() => props.handleClickFav()}
        />
      )}

      {!props.hideCompare && (
        <CompareBtn
          className="goods-detail__btn-icon"
          isActive={props.isGoodsCompare}
          onClick={() => props.handleClickCompare()}
        />
      )}
    </>
  );
};

export default RenderInfo;
