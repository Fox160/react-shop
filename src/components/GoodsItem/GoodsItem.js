import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import RenderInfo from './RenderInfo';

import {
  addToFavorites,
  removeFromFavorites,
} from '../../store/favorites/slice';
import { selectFavoritesItem } from '../../store/favorites/selectors';
import { addToCompare, removeFromCompare } from '../../store/compare/slice';
import { selectCompareItem } from '../../store/compare/selectors';

import cross from '../../assets/img/cross.svg';
import valmona from '../../assets/img/valmona.jpg';
import holika from '../../assets/img/holika.jpg';
import ciracle from '../../assets/img/ciracle.jpg';
import haruharu from '../../assets/img/haruharu.jpg';
import reclar from '../../assets/img/reclar.jpg';
import banila from '../../assets/img/banila.jpg';
import jmsolution from '../../assets/img/jmsolution.jpg';
import missha from '../../assets/img/missha.jpg';
import lador from '../../assets/img/lador.jpg';
import FavBtn from '../FavBtn/FavBtn';
import CompareBtn from '../CompareBtn/CompareBtn';

const images = {
  valmona,
  holika,
  ciracle,
  haruharu,
  reclar,
  banila,
  jmsolution,
  missha,
  lador,
};

const GoodsItem = (props) => {
  const {
    id,
    styleName,
    favBtn,
    compareBtn,
    removeBtn,
    handleClick,
    img,
    name,
    description,
    price,
    available,
  } = props;

  let goodsItemFav = useSelector((state) => selectFavoritesItem(state, id));
  let goodsItemCompare = useSelector((state) => selectCompareItem(state, id));
  const dispatch = useDispatch();

  const handleClickFav = () => {
    if (goodsItemFav) {
      dispatch(removeFromFavorites(id));
    } else {
      dispatch(addToFavorites(id));
    }
  };

  const handleClickCompare = () => {
    if (goodsItemCompare) {
      dispatch(removeFromCompare(id));
    } else {
      dispatch(addToCompare(id));
    }
  };

  return (
    <div className={`goods-item ${styleName}`}>
      {favBtn && (
        <FavBtn
          className="goods-item__fav"
          isActive={goodsItemFav}
          onClick={() => handleClickFav()}
        />
      )}

      {compareBtn && (
        <CompareBtn
          className="goods-item__compare"
          isActive={goodsItemCompare}
          onClick={() => handleClickCompare()}
        />
      )}

      {removeBtn && (
        <button
          className="btn btn--link goods-item__btn-remove"
          onClick={() => handleClick(props)}
        >
          <img src={cross} />
        </button>
      )}
      <img src={images[img]} className="goods-item__img" />

      <div className="goods-item__content">
        <Link to={`/detail/${id}`} className="goods-item__title">
          {name}
        </Link>
        {description && (
          <p className="goods-item__description">{description}</p>
        )}

        {(price || available) && (
          <div className="goods-item__description-info">
            {price && <p className="goods-item__price">{price} ₽</p>}
            {available === false ? (
              <div className="goods-item__text-out goods-item__btn">
                Нет в наличии
              </div>
            ) : (
              <RenderInfo {...props} hideFav hideCompare />
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default GoodsItem;
