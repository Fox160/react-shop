import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { removeFromCart } from '../../store/cart/slice';

import GoodsItem from '../GoodsItem/GoodsItem';

import emptyCart from '../../assets/img/empty-cart.jpg';

const Cart = () => {
  const { cart, goods } = useSelector((state) => state, shallowEqual);
  const dispatch = useDispatch();
  const [sum, setSum] = useState(0);

  useEffect(() => {
    calcSummary();
  }, [cart]);

  const calcSummary = () => {
    let res = 0;

    if (Object.entries(cart).length > 0) {
      res = Object.entries(cart).reduce((acc, [id, itemCount]) => {
        const count = itemCount || 1;
        const price = goods[id] ? goods[id].price : 0;

        acc += price * count;

        return acc;
      }, 0);
    }

    setSum(res);
  };

  return (
    <div
      className={`cart ${!(Object.keys(cart).length > 0) ? 'cart--empty' : ''}`}
    >
      {Object.entries(cart).length > 0 ? (
        <>
          {Object.entries(cart).map(([id, count]) => (
            <GoodsItem
              {...goods[id]}
              count={count}
              key={id}
              id={id}
              handleClick={() => dispatch(removeFromCart(id))}
              removeBtn={true}
              styleName="cart__item goods-item--row"
            />
          ))}

          <div className="cart__actions">
            <div className="cart__summary">
              Итого: <span>{sum} ₽</span>
            </div>

            <Link to="/order" className="btn cart__btn">
              Перейти к оформлению
            </Link>
            <Link to="/" className="btn btn--link cart__btn">
              Вернуться к покупкам
            </Link>
          </div>
        </>
      ) : (
        <>
          <div className="cart__wrapper">
            <p className="cart__title">В корзине пусто &#128532;</p>
            <p className="cart__text">Выберите товары на главной странице!</p>
          </div>
          <img src={emptyCart} alt="Пустая корзина" className="cart__img" />
        </>
      )}
    </div>
  );
};

export default Cart;
