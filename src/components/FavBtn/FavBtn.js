import React from 'react';

import sprite from '../../assets/img/sprite.svg';

const FavBtn = ({ onClick, isActive, className }) => (
  <button
    className={`btn btn--link btn--svg ${className} ${isActive && 'active'}`}
    onClick={onClick}
  >
    <svg>
      <use href={sprite + '#fav'} />
    </svg>
  </button>
);

export default FavBtn;
