const getCardType = (cardNumber) => {
  let re = new RegExp('^4');

  if (cardNumber.match(re) != null) {
    return 'visa';
  }

  re = new RegExp('^5[1-5]');

  if (cardNumber.match(re) != null) {
    return 'mastercard';
  }

  return 'visa'; // default value
};

export default getCardType;
