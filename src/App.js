import React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import GoodsList from './components/GoodsList/GoodsList';
import Cart from './components/Cart/Cart';
import Header from './components/Header/Header';
import OrderForm from './components/Order/OrderForm';
import OrderSuccess from './components/Order/OrderSuccess/OrderSuccess';
import GoodsDetail from './components/GoodsDetail/GoodsDetail';
import FavoritesList from './components/FavoritesList/FavoritesList';
import ComparisonTable from './components/ComparisonTable/ComparisonTable';

const App = () => {
  return (
    <>
      <HashRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={GoodsList} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/order" component={OrderForm} />
          <Route exact path="/order-success" component={OrderSuccess} />
          <Route exact path="/detail/:id" component={GoodsDetail} />
          <Route exact path="/favorites" component={FavoritesList} />
          <Route exact path="/compare" component={ComparisonTable} />
        </Switch>
      </HashRouter>
    </>
  );
};

export default App;
